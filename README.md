### vue-element-admin后台脚手架定制版


### 克隆项目

# 安装依赖
npm install

# 本地开发 启动项目
npm run dev

# 生产环境构建打包
npm run prod
```

| 强烈建议不要用直接使用 cnpm 安装有各种诡异的 bug，可以通过重新指定 registry 来解决 npm 安装速度慢的问题