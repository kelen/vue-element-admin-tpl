export default class Node {
  constructor(options) {
    this.id = options.id
    this.label = options.label
    this.checked = options.checked
    // 半选中
    this.checkHalf = false
    this.parent = options.parent || null
    this.level = options.parent ? options.parent.level + 1 : 0
    this.children = []
  }

  setParent(parent) {
    if (parent) {
      this.parent = parent
      this.level = parent.level + 1
    }
  }
}
