import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '../store'
import { getToken } from '@/utils/auth'

// 创建axios实例
const service = axios.create({
  timeout: 5000 // 请求超时时间
})

// request拦截器
service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      config.headers['X-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    return config
  },
  error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
)

// response 拦截器
service.interceptors.response.use(
  response => {
    const res = response.data
    // 200状态码 code === 401
    if (res.code === 401) {
      store.dispatch('FedLogOut').then(() => {
        location.reload() // 为了重新实例化vue-router对象 避免bug
      })
    }
    return res
  },
  error => {
    let msg = ''
    const status = error.response.status
    if (status === 504) {
      msg = '服务器出错, 请稍后再试'
    }
    // 401状态码
    if (status === 401) {
      msg = '认证失败, 请重新登陆'
    }
    console.log('err' + error) // for debug
    Message({
      message: msg,
      type: 'error',
      duration: 5 * 1000
    })
    if (status === 401) {
      store.dispatch('FedLogOut').then(() => {
        location.reload() // 为了重新实例化vue-router对象 避免bug
      })
    }
    return Promise.reject(error)
  }
)

export default service
