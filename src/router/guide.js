/* Layout */
import Layout from '../views/layout/Layout'

export default {
  path: '/guide',
  component: Layout,
  redirect: '/guide/scenic-list',
  name: '首页',
  meta: {
    title: '导览管理',
    icon: 'home'
  },
  children: [{
    path: 'scenic-list',
    name: '景点列表',
    meta: {
      title: '景点列表'
    },
    component: () => import('@/views/guide/index')
  }, {
    path: 'guide-setting',
    name: '导览配置',
    meta: {
      title: '导览配置'
    },
    component: () => import('@/views/guide/index')
  }]
}
