/* Layout */
import Layout from '../views/layout/Layout'
import guide from './guide'

const routeConfig = [
  {
    path: '/',
    component: Layout,
    redirect: '/guide',
    name: '首页'
  },
  guide,
  { path: '*', redirect: '/404', hidden: true }
]

export default routeConfig
