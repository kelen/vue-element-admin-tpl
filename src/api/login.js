import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: '/api/mgr/pb/login',
    method: 'post',
    data: {
      loginAccount: username,
      password: password
    }
  })
}

// 传入时间随机数避免浏览器缓存
export function geetestInit(time) {
  return request.get('/api/mgr/pb/geeTestInit?t=' + time)
}

export function logout() {

}

export function getInfo() {

}

export default {
  login,
  geetestInit,
  logout,
  getInfo
}
